# FLAST

Flast is a microframework based on
[werkzeug](http://werkzeug.pocoo.org/)

Flast wants to be fast, extensible and easy to maintain. Flast offer
you all you need to be started in web developpment, but only what you
need.

## Requirements

Flast depends on jinja2, werkzeug and optionaly psycopg2. You can
install those either with virtualenv or with your package manager

## Installation

```
pip install flast
```

## Quickstart

Flast come with an utility to get you started easily

```
flast-init test_app
```

this command accept some optionals arguments. see `flast-init --help`

This will create the default files and directories you need to get
started

- test_app
  - app.py
  - settings.py
  - urls.py
  - views.py
  - templates/
  - statics/
  - sql/ (if you use --pg option)

### app.py

This is the python script you need to call when you want to use the
provided server for local testing/debuging

``` python app.py ```

### settings.py

This define all the configuration of you application. Some default
configurations are already set for you:

#### DEBUG

Define if the server should run with the werkzeug debugger <http://werkzeug.pocoo.org/docs/0.11/debug/#using-the-debugger>

`DEFAULT: True`

**do not let this settings in production !**

#### TEMPLATE_DIR

Location of the application template. Relative to the root of your application

`DEFAULT: "templates"`

#### STATIC_DIR

Location of the application statics. This will be used in
developpment. When production time will come, let a real web server
serve your statics (apache, nginx etc...)

`DEFAULT: "statics"`

#### STATIC_PATH

Prefix for statics urls

`DEFAULT: "/static"`

#### POSTGRESQL

in case you use the option `--pg`:

this is a dict containing all the information to connect to a
postgresql database. This dict can contain:

- database: the database name (only in the dsn string)

- host: database host address (defaults to UNIX socket if not
        provided)

- user: user name used to authenticate

- password: password used to authenticate

- port: connection port number (defaults to 5432 if not provided)

### First view

To get you started with a simple application, let's use the default of
`flast-init`

this will create all you need to get started. Then edit views.py


You can de-comment the exemple:


    from flast.views import View


    class Index(View):

        def get(self, request):
            return self.render_template('index.html')


This view will respond to a get request returning the template
"index.html". Because this template does not exists yet you need to
create it.

create and edit `templates/index.html`

    <!doctype html>
    <html class="no-js" lang="">
        <body>
            <p>Hello world!</p>
        </body>
    </html>




The last things to do to make your applcation work is to link your
view to an url. This can be done editing `urls.py`



    from werkzeug.routing import Map, Rule

    url_map = Map([
         Rule('/', endpoint='index'),
         ])


Then you can start you application:

`python app.py`

then use your web browser to go to `<http://localhost:5000>`

### Make things dynamic

It's really easy to make your application dynamic.

Change your views.py to use the query parameters:

    from flast.views import View


    class Index(View):

        def get(self, request):
            return self.render_template('index.html',
                                        username=request.args.get('username'))

Then use this variable in your template:

    <!doctype html>
    <html class="no-js" lang="">
        <body>
            <p>Hello {{username}}</p>
        </body>
    </html>

You can now visit <http://localhost:5000/?username=peter>
