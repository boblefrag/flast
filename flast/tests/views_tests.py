import unittest

from flast.forms import Field, Form, validate_integer
from werkzeug.routing import Map, Rule
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse


class DummyField(Field):

    validators = [validate_integer]


class DummyForm(Form):
    fields = ["something"]

    something = DummyField()


class TestCreateView(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        from testapp.app import App
        from flast.app import create_app
        app = create_app(App)
        from flast.views import CreateView

        url_map = Map([Rule('/', endpoint='create'), ])

        class DummyCreateView(CreateView):

            form = DummyForm
            template = "create.html"
            redirect = "/"

        class views(object):
            def __init__(self):
                self.Create = DummyCreateView

        views = views()
        app.url_map = url_map
        app.views = views
        with open("testapp/templates/create.html", "w") as index:
            index.write("""
<form action="." method="POST">
            {{errors}}
  <input type="text" name="something" />
  <input type="submit" value="save" />
</form>
                """)
        cls.client = Client(app, BaseResponse)

    def test_get_create_view(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue("form" in resp.data.decode('utf-8'))

    def test_post_data(self):
        resp = self.client.post('/', data={"something": 12})
        self.assertEqual(resp.status_code, 302)

        resp = self.client.post('/', data={"something": "nice"})
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(
            "value must be an integer" in resp.data.decode('utf-8')
        )
