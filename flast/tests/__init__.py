import subprocess
import unittest


def get_suite():
    subprocess.call(["flast-init.py", "testapp"])
    return unittest.defaultTestLoader.discover('.', pattern="*_tests.py")
